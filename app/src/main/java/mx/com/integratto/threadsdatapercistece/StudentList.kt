package mx.com.integratto.threadsdatapercistece


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mx.com.integratto.threadsdatapercistece.adapter.StudentHolder
import mx.com.integratto.threadsdatapercistece.database.StudentHelper

/**
 * A simple [Fragment] subclass.
 */
class StudentList : Fragment() {

    private lateinit var recycler: RecyclerView
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_student_list, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let { sharedPreferences = it.getSharedPreferences(SESION, Context.MODE_PRIVATE) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler = view.findViewById(R.id.recycler)
        recycler.setHasFixedSize(true)

        recycler.layoutManager = LinearLayoutManager(activity)
        activity?.let { recycler.adapter = StudentHolder(StudentHelper(it).selectAllStudentByIdUser(sharedPreferences.getInt(ID_USER, 0))) }
    }
}
