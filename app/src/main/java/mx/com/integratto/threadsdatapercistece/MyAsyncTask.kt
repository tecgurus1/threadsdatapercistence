package mx.com.integratto.threadsdatapercistece

import android.os.AsyncTask
import android.os.SystemClock
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

private const val CHANNEL_ID = "EXAMPLE1"
private const val PROGRESS_MAX = 100

class MyAsyncTask(val mainActivity: MainActivity) : AsyncTask<Int, Int, Int>() {

    private lateinit var builder: NotificationCompat.Builder
    private var PROGRESS_CURRENT = 0

    override fun onPreExecute() {
        super.onPreExecute()
        builder = NotificationCompat.Builder(mainActivity, CHANNEL_ID).apply {
            setContentTitle("Calculate in progress")
            setContentText("in progress")
            setSmallIcon(R.drawable.ic_notification)
            priority = NotificationCompat.PRIORITY_LOW
        }
        NotificationManagerCompat.from(mainActivity).apply {

            builder.setContentText("Start example")
                .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false)
            notify(10, builder.build())
        }
    }

    override fun doInBackground(vararg p0: Int?): Int? = p0[0]?.let { calc(it) }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
        values[0]?.let { PROGRESS_CURRENT = it }
        NotificationManagerCompat.from(mainActivity).apply {
            if (PROGRESS_CURRENT < 100) {
                builder.setContentText("Progress example")
                    .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false)
            } else {
                builder.setContentText("End example")
                    .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false)
            }
            notify(10, builder.build())
        }
    }

    override fun onPostExecute(result: Int?) {
        super.onPostExecute(result)
        result?.let { mainActivity.result(it) }
    }

    private fun calc(n: Int): Int {
        var res = 1
        for (i in 1..n ) {
            res*= i
            SystemClock.sleep(1500)
            publishProgress(i * 100 / n)
        }
        return res
    }
}