package mx.com.integratto.threadsdatapercistece.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import mx.com.integratto.threadsdatapercistece.model.User

class UserHelper(context: Context) : Escuela(context) {

    private lateinit var db : SQLiteDatabase

    fun insertUsers() {
        if (selectCountUser() == 0) {
            var cv = ContentValues()
            cv.put("_id", 0)
            cv.put("email", "admin@test.com")
            cv.put("password", "admin")
            cv.put("status", "Activo")
            executeWriteableDB(cv)

            cv = ContentValues()
            cv.put("_id", 0)
            cv.put("email", "yo@lol.com")
            cv.put("password", "123")
            cv.put("status", "Activo")
            executeWriteableDB(cv)
        }
    }

    private fun selectCountUser(): Int {
        val sql = "SELECT COUNT(_id) FROM USER;"
        return executeReadableDB2(sql)?.size ?: 0
    }

    fun selectUser(email: String, password: String): User {
        val sql = "SELECT * FROM USER WHERE email = \'$email\' AND pass = \'$password\' AND status = \'Activo\'"
        return executeReadableDB(sql)?.let { it[0] } ?: User()
    }

    private fun executeReadableDB2(sql: String): MutableList<User>? {
        val listUser: MutableList<User> = mutableListOf()
        val cursor: Cursor
        try {
            db = this.readableDatabase
            cursor = db.rawQuery(sql, null)
            while (cursor.moveToNext()) {
                val user= User()
                user.id = cursor.getInt(0)
                //user.email = cursor.getString(1)
                //user.password = cursor.getString(2)
                //user.status = cursor.getString(3)
                listUser.add(user)
            }
            cursor.close()
        } catch (ex: SQLException) {
            Log.e("UserDB:.::.", ex.message)
            ex.printStackTrace()
        } finally {
            db.close()
        }
        return listUser
    }

    private fun executeReadableDB(sql: String): MutableList<User>? {
        val listUser: MutableList<User> = mutableListOf()
        val cursor: Cursor
        try {
            db = this.readableDatabase
            cursor = db.rawQuery(sql, null)
            while (cursor.moveToNext()) {
                val user= User()
                user.id = cursor.getInt(0)
                user.email = cursor.getString(1)
                user.password = cursor.getString(2)
                user.status = cursor.getString(3)
                listUser.add(user)
            }
            cursor.close()
        } catch (ex: SQLException) {
            Log.e("UserDB:.::.", ex.message)
            ex.printStackTrace()
        } finally {
            db.close()
        }
        return listUser
    }

    private fun executeWriteableDB(cv: ContentValues): Boolean {
        var row: Long
        try {
            db = this.writableDatabase
            row = db.insert("USER", null, cv)
        } catch (ex: SQLException) {
            Log.e("UserDB", ex.message)
            ex.printStackTrace()
            row = 0
        } finally {
            db.close()
        }
        return row > 0
    }
    
}