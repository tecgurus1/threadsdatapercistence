package mx.com.integratto.threadsdatapercistece.model

data class User(var id: Int, var email: String, var password: String, var status: String) {
    constructor(): this(0, "", "", "")
}