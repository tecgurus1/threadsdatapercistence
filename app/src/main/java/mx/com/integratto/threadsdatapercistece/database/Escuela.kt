package mx.com.integratto.threadsdatapercistece.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

open class Escuela(context: Context) : SQLiteOpenHelper(context, "Escuela", null, 1) {

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBEscuela.createTableUser)
        db?.execSQL(DBEscuela.createTableAlumnos)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}
}