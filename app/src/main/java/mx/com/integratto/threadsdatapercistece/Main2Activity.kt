package mx.com.integratto.threadsdatapercistece

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputLayout
import mx.com.integratto.threadsdatapercistece.database.Escuela
import mx.com.integratto.threadsdatapercistece.database.UserHelper

class Main2Activity : AppCompatActivity() {

    private lateinit var edtUser: EditText
    private lateinit var edtPassword: EditText
    private lateinit var btnStart: Button
    private lateinit var textInPutLayout: TextInputLayout
    private lateinit var textInputLayout2: TextInputLayout

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        textInPutLayout = findViewById(R.id.textInputLayout)
        textInputLayout2 = findViewById(R.id.textInputLayout2)

        edtUser = findViewById(R.id.edtUser)
        edtPassword = findViewById(R.id.edtPassword)
        btnStart = findViewById(R.id.btnStart)

        Escuela(this)
        UserHelper(this).insertUsers()

        sharedPreferences = getSharedPreferences(SESION, Context.MODE_PRIVATE)
    }

    override fun onStart() {
        super.onStart()
        if (sharedPreferences.contains(IS_SESION)) {
            startActivity(Intent(this, Main3Activity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        edtUser.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                p0?.let {
                    if (it.toString().isEmpty()) textInPutLayout.isErrorEnabled = false
                }
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        edtPassword.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                p0?.let {
                    if (it.toString().isEmpty()) textInputLayout2.isErrorEnabled = false
                }
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        btnStart.setOnClickListener {
            if (edtUser.text.toString().isNotEmpty()) {
                if (edtPassword.text.toString().isNotEmpty()) {
                    //if (edtUser.text.toString().equals("AdMin", true)) {
                        //if (edtPassword.text.toString().equals("123A", true)) {
                            val user = UserHelper(this).selectUser(edtUser.text.toString(), edtPassword.text.toString())
                            if (user.id > 0) {
                                sharedPreferences.edit().putString(USER, user.email).apply()
                                sharedPreferences.edit().putString(PASSWORD, user.password).apply()
                                sharedPreferences.edit().putInt(ID_USER, user.id).apply()
                                sharedPreferences.edit().putBoolean(IS_SESION, true).apply()
                                startActivity(Intent(this, Main3Activity::class.java))
                            } else {
                                AlertDialog.Builder(this).setTitle("SQLite")
                                    .setMessage("El usuario y/o contaseña son incorrectos")
                                    .setCancelable(false)
                                    .setNeutralButton("Aceptar", null).create().show()
                            }
                        /*} else {
                            textInputLayout2.isErrorEnabled = true
                            textInputLayout2.error = "La contraseña es incporrecta"
                            edtPassword.error = "La contraseña es incorrecta"
                            edtPassword.requestFocus()
                        }*/
                    /*} else {
                        textInPutLayout.isErrorEnabled = true
                        textInPutLayout.error = "El usuario es incorrecto"
                        edtUser.error = "El usuario es incorrecto"
                        edtUser.requestFocus()
                    }*/
                } else {
                    textInputLayout2.isErrorEnabled = true
                    textInputLayout2.error = "El campo se encuentra vacío"
                    edtPassword.error = "El campo se encuentra vacío"
                    edtPassword.requestFocus()
                }
            } else {
                textInPutLayout.isErrorEnabled = true
                textInPutLayout.error = "El campo se encuentra vacío"
                edtUser.error = "El campo se encuentra vacío"
                edtUser.requestFocus()
            }
        }
    }
}
