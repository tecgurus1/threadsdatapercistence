package mx.com.integratto.threadsdatapercistece.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.com.integratto.threadsdatapercistece.R
import mx.com.integratto.threadsdatapercistece.model.Student

class StudentHolder(val listStudent: List<Student>): RecyclerView.Adapter<StudentHolder.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvNombre: TextView = itemView.findViewById(R.id.tvNombre)
        val tvPromedio: TextView = itemView.findViewById(R.id.tvPromedio)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_student_list, parent, false))

    override fun getItemCount(): Int = listStudent.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvNombre.text = listStudent[position].nombre
        holder.tvPromedio.text = listStudent[position].promedio.toString()
    }
}