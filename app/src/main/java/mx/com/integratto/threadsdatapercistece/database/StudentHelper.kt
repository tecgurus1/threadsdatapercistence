package mx.com.integratto.threadsdatapercistece.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import mx.com.integratto.threadsdatapercistece.model.Student

class StudentHelper (context: Context): Escuela(context) {

    lateinit var db: SQLiteDatabase

    fun insertStudent(student: Student): Boolean {
        val cv = ContentValues()
        cv.put("_id", student.id)
        cv.put("idUser", student.idUser)
        cv.put("nombre", student.nombre)
        cv.put("promedio", student.promedio)
        return executeWriteableDB(cv)
    }

    fun selectAllStudentByIdUser(idUser: Int): MutableList<Student> {
        val sql = "SELECT * FROM ALUMNOS WHERE idUser = $idUser"
        return executeReadableDB(sql)?.let { it }?: mutableListOf()
    }

    private fun executeReadableDB(sql: String): MutableList<Student>? {
        val listStudent: MutableList<Student> = mutableListOf()
        val cursor: Cursor
        try {
            db = this.readableDatabase
            cursor = db.rawQuery(sql, null)
            while (cursor.moveToNext()) {
                val student = Student()
                student.id = cursor.getInt(0)
                student.idUser = cursor.getInt(1)
                student.nombre = cursor.getString(2)
                student.promedio = cursor.getDouble(3)
                listStudent.add(student)
            }
            cursor.close()
        } catch (ex: SQLException) {
            Log.e("UserDB:.::.", ex.message)
            ex.printStackTrace()
        } finally {
            db.close()
        }
        return listStudent
    }

    private fun executeWriteableDB(cv: ContentValues): Boolean {
        var row: Long
        try {
            db = this.writableDatabase
            row = db.insert("ALUMNOS", null, cv)
        } catch (ex: SQLException) {
            Log.e("UserDB", ex.message)
            ex.printStackTrace()
            row = 0
        } finally {
            db.close()
        }
        return row > 0
    }
}