package mx.com.integratto.threadsdatapercistece

interface MyAsyncTaskResult {
    fun result(result: Int)
}