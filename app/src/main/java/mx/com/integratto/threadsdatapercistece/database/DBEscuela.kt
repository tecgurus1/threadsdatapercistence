package mx.com.integratto.threadsdatapercistece.database

interface DBEscuela {
    companion object {
        var createTableUser = "CREATE TABLE USER (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " email TEXT, pass TEXT, status TEXT);"

        var createTableAlumnos = "CREATE TABLE ALUMNOS (_id INTEGER PRIMARY KEY AUTOINCREMENT, idUser INTEGER," +
                " nombre TEXT, promedio REAL, FOREIGN KEY (idUSER) REFERENCES USER (_id));"
    }
}