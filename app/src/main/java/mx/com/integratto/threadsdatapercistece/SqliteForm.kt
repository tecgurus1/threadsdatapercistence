package mx.com.integratto.threadsdatapercistece

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import mx.com.integratto.threadsdatapercistece.database.StudentHelper
import mx.com.integratto.threadsdatapercistece.model.Student

/**
 * A simple [Fragment] subclass.
 */
class SqliteForm : Fragment() {

    private lateinit var edtNombre: TextInputEditText
    private lateinit var edtPromedio: TextInputEditText
    private lateinit var btnGuardar: Button
    private lateinit var btnStudentList: Button

    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_sqlite_form, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let { sharedPreferences = it.getSharedPreferences(SESION, Context.MODE_PRIVATE) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edtNombre = view.findViewById(R.id.edtName)
        edtPromedio = view.findViewById(R.id.edtPromedio)
        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnStudentList = view.findViewById(R.id.btnStudenList)

        btnGuardar.setOnClickListener{
            if (edtNombre.text.toString().isNotEmpty()) {
                if (edtPromedio.text.toString().isNotEmpty()) {
                    activity?.let { act ->
                        val student = Student(0, sharedPreferences.getInt(ID_USER, 0), edtNombre.text.toString(), edtPromedio.text.toString().toDouble())
                        if (StudentHelper(act).insertStudent(student)) it.findNavController().navigate(R.id.action_sqliteFrom_to_studentList)
                        else AlertDialog.Builder(act).setTitle("SQLite")
                            .setMessage("Ocurrio un error favor de revisar el log")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", null).create().show()
                    }
                }
            }
        }

        btnStudentList.setOnClickListener {
            it.findNavController().navigate(R.id.action_sqliteFrom_to_studentList)
        }
    }
}
