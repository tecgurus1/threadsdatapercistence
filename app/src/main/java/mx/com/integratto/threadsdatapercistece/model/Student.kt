package mx.com.integratto.threadsdatapercistece.model

data class Student(var id: Int, var idUser: Int, var nombre: String, var promedio: Double) {
    constructor(): this(0, 0, "", 0.0)
}