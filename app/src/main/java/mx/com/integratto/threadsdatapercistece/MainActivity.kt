package mx.com.integratto.threadsdatapercistece

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MyAsyncTaskResult {

    companion object {
        private lateinit var tvTxt: TextView
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        tvTxt = findViewById(R.id.tvTxt)
        fab.setOnClickListener {
            /*try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }*/

            /*Handler().postDelayed({
                Toast.makeText(this, "Este es otro hilo", Toast.LENGTH_SHORT).show()
            }, 2000)*/

            /*val my = MyAsyncTask(this)
            my.execute(5)*/
            MyAsyncTask(this).execute(3)
            /*val myThread = MyThread()
            myThread.start()*/
            /*val txt = "Nuevo texto"
            Thread{
                for (i in 0..30 step 2) {
                    Log.e("ThreadAnonim", "Secuencia en la pos: $i")
                    //var txt = "Nuevo texto"
                    /*tvTxt.post{
                        tvTxt.text = txt
                    }*/
                    runOnUiThread {
                        tvTxt.text = txt
                    }
                }
            }.start()*/
        }

        findViewById<Button>(R.id.btnDataPreference).setOnClickListener {
            startActivity(Intent(this, Main2Activity::class.java))
        }
    }

    class MyThread : Thread() {
        override fun run() {
            super.run()
            for (x in 0 until 10) {
                /*try {
                    sleep(2000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }*/
                Log.e("MyThread", "Ejecutandoce en la pos: $x")

                tvTxt.post {
                    tvTxt.text = "Cambio en el hilo"
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun result(result: Int) {
        tvTxt.text = "El resultado es $result"
    }
}
